class GES:
    def __init__(self, KPD, QMax, ZBbN, GESlevel):
        self.KPD = KPD
        self.QMax = QMax
        self.Qp = 0
        self.ZBbN = ZBbN
        self.ZBbKx = 0
        self.ZBbKy = 0
        self.Qb = 0
        self.QgesMin = 0
        self.QgesMax = 0
        self.ZBbK = self.ZBbN
        self.Qges = 0
        self.QX = 0
        self.GESlevel = GESlevel
        self.NgesArr = []
        self.QXArr = []
        self.ZBbKArr = []
        self.QgesArr = []
        self.HgesArr = []

    def findQges(self):
        # Поиск максимума Nges, зависящего от отмеки водохранилища , при ограничениях
        # Qges [QgesMin; QgesMax]
        N = 100 
        NgesMax = 0
        ZBbKMax = 0
        QgesMax = 0
        HgesMax = 0


        for i in range(0, N):
            ZBbKCur = self.ZBbKx + float(self.ZBbKy - self.ZBbKx) / N * i
            QgesCur = float(0.15 * self.ZBbN + self.Qp - 0.15 * ZBbKCur)
            if QgesCur >= self.QgesMin and QgesCur <= self.QgesMax:

                if self.ZBbNNext == 0:
                    HgesCur = ZBbKCur
                else:
                    HgesCur = self.GESlevel + float(ZBbKCur - self.ZBbNNext)

                NgesCur = float(9.81 * QgesCur * abs(HgesCur) * self.KPD)
                if NgesCur > NgesMax:
                    NgesMax = NgesCur
                    QgesMax = QgesCur
                    HgesMax = HgesCur
                    ZBbKMax = ZBbKCur

        if QgesMax == 0:
            ZBbKMax = self.ZBbKx
            QgesMax = abs(float(0.15 * self.ZBbN + self.Qp - 0.15 * ZBbKMax))
            if self.ZBbNNext == 0:
                HgesMax = self.ZBbKy
                ZBbKMax = self.ZBbKy
                QgesMax = abs(float(0.15 * self.ZBbN + self.Qp - 0.15 * ZBbKMax))
            else: 
                HgesMax = float(ZBbKMax - self.ZBbNNext  - (QgesCur + self.Qb))  + self.GESlevel
            NgesMax = float(9.81 * QgesMax * abs(HgesMax) * self.KPD)

        return (NgesMax, QgesMax, HgesMax, ZBbKMax)


    def nextMonth(self, Qp, ZBbKx, ZBbKy, Qb, QgesMin, QgesMax, ZBbNNext):
        self.Qp = Qp
        self.ZBbN = self.ZBbK
        self.ZBbKx = ZBbKx
        self.ZBbKy = ZBbKy
        self.Qb = Qb
        self.QgesMin = QgesMin
        self.QgesMax = QgesMax
        self.ZBbNNext = ZBbNNext

    def calcNGes(self):
        NgesMax, QgesMax, HgesMax, ZBbKMax = self.findQges()

        # QX - холостые сбросы
        if QgesMax > self.QMax:
            QX = QgesMax - self.QMax
            QgesMax = self.QMax
        else:
            QX = 0


        self.Nges = NgesMax
        self.Qges = QgesMax
        self.QX = QX
        self.ZBbK = ZBbKMax
 

        self.NgesArr.append(self.Nges)
        self.QgesArr.append(self.Qges)
        self.QXArr.append(self.QX)
        self.ZBbKArr.append(self.ZBbK)
        self.HgesArr.append(HgesMax)

    def printState(self):
        print('Начальный уровень воды', self.ZBbN, \
              '\nНачальный объём воды', self.ZBbN * 0.15, \
              '\nСброс', self.Qges, \
              '\nУровень воды при этом сбросе', self.ZBbK, \
              '\nОбъем в баке при этом уровне', self.ZBbK * 0.15, \
              '\nQ холостого сброса', self.QX)


class Requirements:
    def __init__(self, Qp, ZBbKx, ZBbKy, Qb, QgesMin, QgesMax):
        self.Qp = Qp
        self.ZBbKx = ZBbKx
        self.ZBbKy = ZBbKy
        self.Qb = Qb
        self.QgesMin = QgesMin
        self.QgesMax = QgesMax
