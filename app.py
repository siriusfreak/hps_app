import os

from flask import Flask, render_template, jsonify, request
from ges import GES, Requirements

app = Flask(__name__, instance_relative_config=True)

requirements = [[Requirements(Qp=3.3, ZBbKx=15, ZBbKy=18, Qb=2.9, QgesMin=0.5, QgesMax=3),
                 Requirements(Qp=0, ZBbKx=15, ZBbKy=17, Qb=2.8, QgesMin=0.5, QgesMax=3),
                 Requirements(Qp=0, ZBbKx=11, ZBbKy=13, Qb=0, QgesMin=0.5, QgesMax=3)],
                [Requirements(Qp=3.3, ZBbKx=20, ZBbKy=23, Qb=2.9, QgesMin=0.5, QgesMax=6),
                 Requirements(Qp=0, ZBbKx=18, ZBbKy=23, Qb=2.8, QgesMin=0.5, QgesMax=6),
                 Requirements(Qp=0, ZBbKx=18, ZBbKy=22, Qb=0, QgesMin=0.5, QgesMax=6)],
                [Requirements(Qp=1.3, ZBbKx=24, ZBbKy=28, Qb=0.8, QgesMin=0.5, QgesMax=6),
                 Requirements(Qp=0, ZBbKx=26, ZBbKy=30, Qb=0.9, QgesMin=0.5, QgesMax=6),
                 Requirements(Qp=0, ZBbKx=23, ZBbKy=27, Qb=0, QgesMin=0.5, QgesMax=6)],
                [Requirements(Qp=0.9, ZBbKx=24, ZBbKy=28, Qb=0.6, QgesMin=0.5, QgesMax=6),
                 Requirements(Qp=0, ZBbKx=26, ZBbKy=30, Qb=0.5, QgesMin=0.5, QgesMax=6),
                 Requirements(Qp=0, ZBbKx=23, ZBbKy=27, Qb=0, QgesMin=0.5, QgesMax=6)],
                [Requirements(Qp=0.9, ZBbKx=24, ZBbKy=28, Qb=0.6, QgesMin=0.5, QgesMax=6),
                 Requirements(Qp=0, ZBbKx=26, ZBbKy=30, Qb=0.5, QgesMin=0.5, QgesMax=6),
                 Requirements(Qp=0, ZBbKx=23, ZBbKy=27, Qb=0, QgesMin=0.5, QgesMax=6)],
                [Requirements(Qp=0.9, ZBbKx=24, ZBbKy=28, Qb=0.6, QgesMin=0.5, QgesMax=6),
                 Requirements(Qp=0, ZBbKx=26, ZBbKy=30, Qb=0.5, QgesMin=0.5, QgesMax=6),
                 Requirements(Qp=0, ZBbKx=23, ZBbKy=27, Qb=0, QgesMin=0.5, QgesMax=6)],
                [Requirements(Qp=1.3, ZBbKx=24, ZBbKy=28, Qb=0.8, QgesMin=0.5, QgesMax=6),
                 Requirements(Qp=0, ZBbKx=26, ZBbKy=30, Qb=0.9, QgesMin=0.5, QgesMax=6),
                 Requirements(Qp=0, ZBbKx=23, ZBbKy=27, Qb=0, QgesMin=0.5, QgesMax=6)],
                [Requirements(Qp=1.3, ZBbKx=24, ZBbKy=28, Qb=0.8, QgesMin=0.5, QgesMax=6),
                 Requirements(Qp=0, ZBbKx=26, ZBbKy=30, Qb=0.9, QgesMin=0.5, QgesMax=6),
                 Requirements(Qp=0, ZBbKx=23, ZBbKy=27, Qb=0, QgesMin=0.5, QgesMax=6)],
                [Requirements(Qp=0.9, ZBbKx=20, ZBbKy=28, Qb=0.6, QgesMin=0.5, QgesMax=6),
                 Requirements(Qp=0, ZBbKx=20, ZBbKy=26, Qb=0.5, QgesMin=0.5, QgesMax=6),
                 Requirements(Qp=0, ZBbKx=20, ZBbKy=27, Qb=0, QgesMin=0.5, QgesMax=6)],
                [Requirements(Qp=0.9, ZBbKx=18, ZBbKy=22, Qb=0.6, QgesMin=0.5, QgesMax=6),
                 Requirements(Qp=0, ZBbKx=16, ZBbKy=18, Qb=0.5, QgesMin=0.5, QgesMax=6),
                 Requirements(Qp=0, ZBbKx=18, ZBbKy=20, Qb=0, QgesMin=0.5, QgesMax=6)],
                [Requirements(Qp=0.4, ZBbKx=16, ZBbKy=20, Qb=0.3, QgesMin=0.5, QgesMax=6),
                 Requirements(Qp=0, ZBbKx=12, ZBbKy=13, Qb=0.3, QgesMin=0.5, QgesMax=6),
                 Requirements(Qp=0, ZBbKx=14, ZBbKy=15, Qb=0, QgesMin=0.5, QgesMax=6)],
                [Requirements(Qp=1.3, ZBbKx=14, ZBbKy=14, Qb=0.8, QgesMin=0.5, QgesMax=6),
                 Requirements(Qp=0, ZBbKx=8, ZBbKy=8, Qb=0.9, QgesMin=0.5, QgesMax=6),
                 Requirements(Qp=0, ZBbKx=11, ZBbKy=11, Qb=0, QgesMin=0.5, QgesMax=6)],
                ]


@app.route('/calc', methods=['GET', 'POST'])
def calc():
    ges1 = GES(KPD=float(request.values.get("KPD_1")), QMax=float(request.values.get("QMax_1")), 
        ZBbN=float(request.values.get("ZBbN_1")), GESlevel = float(request.values.get("GESlevel_1")))

    ges2 = GES(KPD=float(request.values.get("KPD_2")), QMax=float(request.values.get("QMax_2")), 
       ZBbN=float(request.values.get("ZBbN_2")), GESlevel = float(request.values.get("GESlevel_1")))

    ges3 = GES(KPD=float(request.values.get("KPD_3")), QMax=float(request.values.get("QMax_3")), 
      ZBbN=float(request.values.get("ZBbN_3")), GESlevel = float(request.values.get("GESlevel_1")))

    for i in range(0, len(requirements)):
        ges1.nextMonth(Qp=requirements[i][0].Qp, ZBbKx=requirements[i][0].ZBbKx, ZBbKy=requirements[i][0].ZBbKy,
                       Qb=requirements[i][0].Qb, QgesMin=requirements[i][0].QgesMin,
                       QgesMax=requirements[i][0].QgesMax, ZBbNNext=ges2.ZBbN)
        ges2.nextMonth(Qp=ges1.Qges + ges1.QX + requirements[i][1].Qb, ZBbKx=requirements[i][1].ZBbKx, ZBbKy=requirements[i][1].ZBbKy,
                       Qb=requirements[i][1].Qb, QgesMin=requirements[i][1].QgesMin,
                       QgesMax=requirements[i][1].QgesMax, ZBbNNext=ges3.ZBbN)
        ges3.nextMonth(Qp=ges2.Qges + ges2.QX + requirements[i][2].Qb, ZBbKx=requirements[i][2].ZBbKx, ZBbKy=requirements[i][2].ZBbKy,
                       Qb=requirements[i][2].Qb, QgesMin=requirements[i][2].QgesMin,
                       QgesMax=requirements[i][2].QgesMax, ZBbNNext=0)

        print("Расчёт режимов работы месяца: " + str(i + 1))
        ges1.calcNGes()
        print('Режим работые первой ГЭС: ')
        ges1.printState()

        ges2.calcNGes()
        print('Режим работые второй ГЭС: ')
        ges2.printState()

        ges3.calcNGes()
        print('Режим работые третьей ГЭС: ')
        ges3.printState()

    return jsonify({
        "GES_1": {
            'Nges': ges1.NgesArr,
            'Qges': ges1.QgesArr,
            'QX': ges1.QXArr,
            'ZBbK': ges1.ZBbKArr,
            'Hges': ges1.HgesArr,
        },
        "GES_2": {
            'Nges': ges2.NgesArr,
            'Qges': ges2.QgesArr,
            'QX': ges2.QXArr,
            'ZBbK': ges2.ZBbKArr,
            'Hges': ges2.HgesArr,
        },
        "GES_3": {
            'Nges': ges3.NgesArr,
            'Qges': ges3.QgesArr,
            'QX': ges3.QXArr,
            'ZBbK': ges3.ZBbKArr,
            'Hges': ges3.HgesArr,
        }
    })


@app.route('/')
def hello():
    return render_template('index.html')
